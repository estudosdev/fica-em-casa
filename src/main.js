import companys from './data'

function main(){  
  const trBase = (data) => `
      <tr>
        <td class="text-center">${data.name}</td>
        <td class="text-center"><button class="btn-info">Ver produtos</button></td>
        <td class="text-center"><button class="btn-contact">Whatsapp</button></td>
      </tr>
  `;

  const bodyTable = document.querySelector('.body-table');
  const bodyTableMobile = document.querySelector('.body-table.mobile');
  
  companys.map(company => {
    bodyTable.innerHTML += trBase(company)
  })

  companys.map(company => {
    bodyTableMobile.innerHTML += trBase(company)
  })
}

main()